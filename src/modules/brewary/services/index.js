import api from '../network'

export function getAllBreweries () {
    return api.get('/breweries');
}
