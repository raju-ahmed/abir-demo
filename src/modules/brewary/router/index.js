import BrewHome from '../components/BrewHome'
const brewRoutes = [
    {
        path: '',
        component: BrewHome,
        name: 'brew:home'
    }
];
export default brewRoutes;
