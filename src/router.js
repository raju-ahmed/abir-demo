import Vue from 'vue'
import Router from 'vue-router'
import brewRoutes from '@/modules/brewary/router'
const baseRoutes = [
    {
        path: '/',
        name: 'index',
        redirect: {name: 'brew:home'}
    }
];

var routes = baseRoutes.concat(brewRoutes)
Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: routes
})
